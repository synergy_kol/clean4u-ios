import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuykitsPage } from './buykits.page';

const routes: Routes = [
  {
    path: '',
    component: BuykitsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuykitsPageRoutingModule {}
