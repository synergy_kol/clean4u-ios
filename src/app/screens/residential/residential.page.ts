import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-residential',
  templateUrl: './residential.page.html',
  styleUrls: ['./residential.page.scss'],
})
export class ResidentialPage implements OnInit {

  popup = false;
  data: any = [];
  isLoading = false;
  isDisabled: boolean = false;

  name: any = "";
  no_bedroom: any = "";
  no_half_bathroom: any = "";
  no_full_bathroom: any = "";
  no_sitting_room: any = "";
  no_kitchen: any = "";
  no_large_kitchen: any = "";
  no_addi_room: any = "";
  no_glass_panel: any = "";
  no_stairways: any = "";
  addi_amount: any = "";
  addi_note: any = "";
  charge_method1: any = 0;

  sqft: any = "";
  rate = 500;
  man_hours: any = 0;
  hour_rate = 45;
  charge_method2: any = 0;

  email: any = "";

  constructor(
    private loadingController: LoadingController,
    private navCtrl: NavController,
    private storage: Storage,
    private authService: AuthServiceProvider,
    private toastController: ToastController,
  ) { }

  ngOnInit() {
  }

  method1Val() {
    var cal1val = Number(this.no_bedroom) * 10 + Number(this.no_half_bathroom) * 15 + Number(this.no_full_bathroom) * 30 + Number(this.no_sitting_room) * 10 + Number(this.no_kitchen) * 15 + Number(this.no_large_kitchen) * 15 + Number(this.no_addi_room) * 7 + Number(this.no_glass_panel) * 2 + Number(this.no_stairways) * 2 + Number(this.addi_amount);
    this.charge_method1 = "$ " + cal1val.toFixed(2);   
  }

  method2Val() {
    var cal1val = Number(this.sqft) / Number(this.rate);
    this.man_hours = cal1val.toFixed(2);
    var finalrate = Number(this.man_hours) * Number(this.hour_rate)
    this.charge_method2 = "$ " + finalrate.toFixed(2);
  }


  onSubmit() {
    const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.name.trim() == "") {
      this.presentToast("Please enter Name of Residence.");
    } else if (this.sqft.trim() == "") {
      this.presentToast("Please Enter Sqft.");
    } else if (String(this.rate).trim() == "") {
      this.presentToast("Please Enter your Production Rate.");
    } else if (String(this.hour_rate).trim() == "") {
      this.presentToast("Please Enter Your Hourly Charge Rate.");
    } else if (this.email.trim() == "") {
      this.presentToast("Please enter Email.");
    } else if (!emailPattern.test(this.email)) {
      this.presentToast('Wrong Email Format...');
    } else {
      this.showLoader('Please wait...');
      this.storage.get('userDetails').then((val) => {
        this.data = val;
        var body = {
          source: "mobile",
          user_id: this.data.user_id,
          name_of_residence: this.name,
          no_of_bedrooms: this.no_bedroom,
          no_of_half_bathroom: this.no_half_bathroom,
          no_of_full_bathroom: this.no_full_bathroom,
          no_of_sitting_rooms: this.no_sitting_room,
          no_of_kitchen: this.no_kitchen,
          no_of_large_kitchen: this.no_large_kitchen,
          no_of_additional_room: this.no_addi_room,
          no_of_glass_panel: this.no_glass_panel,
          no_of_stairways: this.no_stairways,
          no_of_additional_item: this.addi_amount,
          additional_note: this.addi_note,
          charge_per_visit: this.charge_method1,
          sqrt_area: this.sqft,
          production_rate: this.rate,
          estimated_man_hours: this.man_hours,
          hourly_charge_rate: this.hour_rate,
          total_charge_per_visit: this.charge_method2,
          email: this.email
        };
        this.authService.postData("Services/SendResidanceCalculationEmail", body).then(result => {
          this.data = result;
          console.log(this.data);
          if (this.data.success == true) {
            this.hideLoader();
            this.popup = true;
          } else {
            this.presentToast(this.data.message);
          }
        },
          error => {
            this.hideLoader();
          });
      });
    }
  }

  onContinue() {
    this.popup = false;
    this.navCtrl.navigateRoot('/home');
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
