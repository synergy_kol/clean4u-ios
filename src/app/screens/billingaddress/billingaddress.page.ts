import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-billingaddress',
  templateUrl: './billingaddress.page.html',
  styleUrls: ['./billingaddress.page.scss'],
})
export class BillingaddressPage implements OnInit {

  data: any = [];

  isDisabled: boolean = false;
  isLoading = false;

  fname: any = '';
  lname: any = '';
  phone: any = '';
  email: any = '';
  address: any = '';
  address1: any = '';
  stateList: any = [];
  state: any;
  cityList: any = [];
  city: any;
  pincode: any = '';

  newAdd = false;
  alladdress: any = [];
  preAdd: any = '';
  addId: any = '';

  constructor(
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private alertController: AlertController
  ) { }

  statePopoverOptions: any = {
    header: 'Please Select State'
  };
  cityPopoverOptions: any = {
    header: 'Please Select City'
  };

  onKeyboard(event) {
    if (event != null) {
      event.setFocus();
    } else {
      this.onNext();
    }
  }


  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getState();
    this.getDetails();
  }

  getState() {
    var StateBody = {
      source: "mobile"
    };
    this.authService.postData("Services/StateList", StateBody).then(result => {
      this.data = result;
      console.log("State: ", this.data);
      this.stateList = this.data.data;
      this.hideLoader();      
    },
      error => {
        this.hideLoader();
      });
  }

  onStatechange() {
    this.showLoader('Please wait...');
    var CityBody = {
      source: "mobile",
      state_code: this.state
    };
    this.authService.postData("Services/CityList", CityBody).then(result => {
      this.data = result;
      console.log("City: ", this.data);
      this.cityList = this.data.data;
      this.city = null;
      this.hideLoader();
    },
      error => {
        this.hideLoader();
      });
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      console.log(this.data);
      var body = {
        source: "mobile",
        user_id: this.data.user_id
      };
      this.authService.postData("api/user/addresslist", body).then(result => {
        this.data = result;
        console.log("Address: ", this.data);
        this.alladdress = this.data.data;
        if(this.alladdress == null){
          this.newAdd = true;
        }
        this.hideLoader();
        this.preAdd = '';
      },
        error => {
          this.hideLoader();
        });
    });
  }

  onNew() {
    this.newAdd = !this.newAdd;
  }

  useAddress() {
    this.fname = this.preAdd.firstname;
    this.lname = this.preAdd.lastname;
    this.phone = this.preAdd.phone_no;
    this.email = this.preAdd.email;
    this.address = this.preAdd.street_address_line1;
    this.address1 = this.preAdd.street_address_line2;
    this.state = this.preAdd.state_id;
    this.city = this.preAdd.city_id;
    this.pincode = this.preAdd.pincode;
    this.addId = this.preAdd.address_id;
  }

  onRemove(id) {
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, want you Remove this Address?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Please wait...');
            this.storage.get('userDetails').then((val) => {
              this.data = val;
              var body = {
                source: "mobile",
                user_id: this.data.user_id,
                address_id: id
              };
              this.authService.postData("api/user/addressremove", body).then(result => {
                this.hideLoader();
                this.data = result;
                console.log(this.data);
                if (this.data.success == true) {
                  this.presentToast("Address Successfully Removed.");
                  this.getDetails();
                } else {
                  this.presentToast(this.data.message);
                }
              },
                error => {
                  this.hideLoader();
                });
            });
          }
        }
      ]
    }).then(a => {
      a.present();
    });    
  }

  onNext() {
    this.showLoader('Please wait...');
    setTimeout(() => {
      this.hideLoader();
      this.presentToast("Successfully Proceeded...");
      var object = {
        addressId: this.addId
      }
      this.navCtrl.navigateForward(["/payment"], { queryParams: object });
    }, 1000);    
  }

  onSaveContinue() {
    const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const phonePattern = /[^0-9]/;
    const namePattern = /[^a-z A-Z]/;
    if (this.fname.trim() == '') {
      this.presentToast('Please enter your First Name');
    } else if (namePattern.test(this.fname)) {
      this.presentToast('First Name field should be Only Letters...');
    } else if (this.lname.trim() == '') {
      this.presentToast('Please enter your Last Name');
    } else if (namePattern.test(this.lname)) {
      this.presentToast('Last Name field should be Only Letters...');
    } else if (this.phone.trim() == '') {
      this.presentToast('Please enter Phone Number');
    } else if (phonePattern.test(this.phone)) {
      this.presentToast('Phone field should be Only Numbers...');
    } else if (this.phone.length <= 9) {
      this.presentToast('Phone! Please enter minimum 10 Numbers');
    } else if (this.email.trim() == '') {
      this.presentToast('Please enter your Email ID');
    } else if (!emailPattern.test(this.email)) {
      this.presentToast('Wrong Email Format...');
    } else if (this.address.trim() == '') {
      this.presentToast('Please enter your Address');
    } else if (this.address1.trim() == '') {
      this.presentToast('Please enter your Address 1');
    } else if (this.state == null) {
      this.presentToast('Please Select State');
    } else if (this.city == null) {
      this.presentToast('Please Select City');
    } else if (this.pincode.trim() == '') {
      this.presentToast('Please enter your Pin Code');
    } else {
      this.showLoader('Please wait...');
      this.storage.get('userDetails').then((val) => {
        this.data = val;
        var body = {
          source: "mobile",
          user_id: this.data.user_id,
          first_name: this.fname,
          last_name: this.lname,
          email: this.email,
          phone_no: this.phone,
          street_address_line1: this.address,
          street_address_line2: this.address1,
          state_id: this.state,
          city_id: this.city,
          pincode: this.pincode
        };
        this.authService.postData("api/product/addaddress", body).then(result => {
          this.hideLoader();
          this.data = result;
          console.log(this.data);
          if (this.data.success == true) {
            this.presentToast("Successfully Proceeded... Address Saved for Future Use.");
            var object = {
              addressId: this.data.data
            }
            this.navCtrl.navigateForward(["/payment"], { queryParams: object });
            this.newAdd = false;
          } else {
            this.presentToast(this.data.message);
          }
        },
          error => {
            this.hideLoader();
          });
      });
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
