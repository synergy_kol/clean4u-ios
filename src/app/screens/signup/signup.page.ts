import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  fname: any = '';
  lname: any = '';
  phone: any = '';
  email: any = '';
  password: any = '';
  cnfpassword: any = '';
  termscondit = false;

  data: any = [];

  isDisabled: boolean = false;
  isLoading = false;

  constructor(
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private authService: AuthServiceProvider
  ) { }

  onKeyboard(event) {
    if (event != null) {
      event.setFocus();
    } else {
      this.onSignup();
    }
  }

  ngOnInit() {
  }

  onSignup(){
    const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const phonePattern = /[^0-9]/;
    const namePattern = /[^a-z A-Z]/;
    const passPattern = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}/;
    if (this.fname.trim() == '') {
      this.presentToast('Please enter your First Name');
    } else if (namePattern.test(this.fname)) {
      this.presentToast('First Name field should be Only Letters...');
    } else if (this.lname.trim() == '') {
      this.presentToast('Please enter your Last Name');
    } else if (namePattern.test(this.lname)) {
      this.presentToast('Last Name field should be Only Letters...');
    } else if (this.phone.trim() == '') {
      this.presentToast('Please enter Phone Number');
    } else if (phonePattern.test(this.phone)) {
      this.presentToast('Phone field should be Only Numbers...');
    } else if (this.phone.length <= 9) {
      this.presentToast('Phone! Please enter minimum 10 Numbers');
    } else if (this.email.trim() == '') {
      this.presentToast('Please enter your Email ID');
    } else if (!emailPattern.test(this.email)) {
      this.presentToast('Wrong Email Format...');
    } else if (this.password.trim() == '') {
      this.presentToast('Please enter your Password');
    } else if (this.password.length < 8 || !passPattern.test(this.password)){
      this.presentToast('Password must be contain minimum 8 letters and atleast one lowercase and one uppercase letter one digit and one special character.');
    } else if (this.cnfpassword.trim() == '') {
      this.presentToast('Please enter confirm password');
    } else if (this.cnfpassword != this.password) {
      this.presentToast("Password and Confirm Password don't match.");
    } else if (this.termscondit == false) {
      this.presentToast('Please check Terms and Conditions');
    } else {
      this.showLoader('Please wait...');
      var body = {
        source: "mobile",
        first_name: this.fname,
        last_name: this.lname,
        email: this.email,
        phone_number: this.phone,
        password: this.password,
        device_id: ""        
      }
      this.authService.postData("Services/Signup", body).then(result => {
        this.hideLoader();
        this.data = result;
        console.log(this.data);
        if (this.data.success == true) {          
          this.presentToast("Thanks! You has been succesfully Registered.");
          this.navCtrl.navigateRoot('/successregistration');
        } else {
          this.presentToast(this.data.status.message);
        }
      },
        error => {
          this.hideLoader();
        });
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
