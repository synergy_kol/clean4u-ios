import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvoicesettingsPage } from './invoicesettings.page';

const routes: Routes = [
  {
    path: '',
    component: InvoicesettingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvoicesettingsPageRoutingModule {}
