import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-forgotpass',
  templateUrl: './forgotpass.page.html',
  styleUrls: ['./forgotpass.page.scss'],
})
export class ForgotpassPage implements OnInit {

  email: any = '';

  data: any = [];
  userData: any;

  isDisabled: boolean = false;
  isLoading = false;

  constructor(
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private authService: AuthServiceProvider,
    private storage: Storage,
  ) { }

  ngOnInit() {
  }

  onSend() {
    const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.email.trim() == '') {
      this.presentToast('Please enter your registered Email ID');
    } else if (!emailPattern.test(this.email)) {
      this.presentToast('Wrong Email Format...');
    } else {
      this.showLoader('Please wait...');
      var body = {
        source: "mobile",
        email: this.email
      };
      this.authService.postData("Services/forgotpassword", body).then(result => {
        this.data = result;
        this.userData = this.data.data;
        console.log("Forgot Pass: ", this.data);
        this.hideLoader();
        if (this.data.success == true) {
          this.presentToast("Password Reset Link Send to Your Mail. Please Check Your Inbox or Span");
          this.navCtrl.navigateRoot("/login")
        } else {
          this.presentToast(this.data.message);
        }
      },
        error => {
          this.hideLoader();
        });
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
