import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-help',
  templateUrl: './help.page.html',
  styleUrls: ['./help.page.scss'],
})
export class HelpPage implements OnInit {

  data: any = [];
  heplData: any = [];
  isLoading = false;

  constructor(
    private authService: AuthServiceProvider,
    private loadingController: LoadingController
  ) { }

  ngOnInit() {
  }

  expandItem(i) {
    this.heplData[i].open = !this.heplData[i].open;
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    var body = {};
    this.authService.postData("Services/GetHelpList", body).then(result => {
      this.data = result;
      this.heplData = this.data.data;
      this.hideLoader();
    },
      error => {
        this.hideLoader();
      });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
