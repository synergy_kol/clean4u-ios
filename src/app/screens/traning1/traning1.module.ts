import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Traning1PageRoutingModule } from './traning1-routing.module';

import { Traning1Page } from './traning1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Traning1PageRoutingModule
  ],
  declarations: [Traning1Page]
})
export class Traning1PageModule {}
