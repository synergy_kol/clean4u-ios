import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  isLoading = false;

  data: any = [];
  incart: any = "";

  constructor(
    private alertController: AlertController,
    private storage: Storage,
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private authService: AuthServiceProvider
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var cartqnt = {
        source: "mobile",
        user_id: this.data.user_id
      };
      this.authService.postData("api/product/cartcount", cartqnt).then(result => {
        this.data = result;
        this.incart = this.data.data;
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  onLogout() {
    const alert = this.alertController.create({
      header: 'Confirm Logout!',
      message: 'Are you sure, you want to Logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Logging out...');
            setTimeout(() => {
              this.storage.clear();
              this.hideLoader();
              this.navCtrl.navigateRoot('/login');
            }, 1000);
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }
}
