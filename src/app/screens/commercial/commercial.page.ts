import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-commercial',
  templateUrl: './commercial.page.html',
  styleUrls: ['./commercial.page.scss'],
})
export class CommercialPage implements OnInit {

  popup = false;
  data: any = [];
  isLoading = false;
  isDisabled: boolean = false;

  account: any = "";
  CA: any = "";
  PR_CA: any = "";
  ET_CA: any = 0;
  HA: any = "";
  PR_HA: any = "";
  ET_HA: any = 0;
  numofTUMS: any = "";
  EHC: any = 0;

  WPH: any = "";
  RPH: any = 0;

  PWCF: any = 0;
  finalbid: any = "";

  email: any = "";

  constructor(
    private loadingController: LoadingController,
    private navCtrl: NavController,
    private storage: Storage,
    private authService: AuthServiceProvider,
    private toastController: ToastController
  ) { }

  customPopoverOptions: any = {
    header: 'Select Days per Week'
  };

  onKeyboard(event) {
    if (event != null) {
      event.setFocus();
    } else {
      this.onSubmit();
    }
  }

  ngOnInit() {
  }

  caVal() {
    if (Number(this.CA) < Number(this.PR_CA)) {
      alert("Your PR For The Carpeted Area should be less than Carpeted Area");
      this.PR_CA = "";
      this.ET_CA = 0;
    } else {
      var caTotal = Number(this.CA) / Number(this.PR_CA);
      this.ET_CA = caTotal.toFixed(2);
      if(this.CA == "" || this.PR_CA == ""){
        this.ET_CA = 0;
      } else {
        this.EHC = Number(this.ET_CA) + Number(this.ET_HA);
        this.tumsVal();
      }      
    }
  }

  haVal() {
    if (Number(this.HA) < Number(this.PR_HA)) {
      alert("Your PR For The Hard Area should be less than Hard Floor Area");
      this.PR_HA = "";
      this.ET_HA = 0;
    } else {
      var haTotal =Number(this.HA) / Number(this.PR_HA);
      this.ET_HA = haTotal.toFixed(2);
      if(this.HA == "" || this.PR_HA == ""){
        this.ET_HA = 0;
      } else {
        this.EHC = Number(this.ET_HA) + Number(this.ET_CA);
        this.tumsVal();
      }       
    }
  }

  tumsVal() {
    var caldata = Number(this.numofTUMS) * 4 / 100;
    var calEHC = Number(this.ET_CA) + Number(this.ET_HA) + caldata;
    this.EHC = calEHC.toFixed(2);
    this.pwcfVal();
  }

  wphVal(){
    var calRPH = 2.10 * Number(this.WPH);
    this.RPH = calRPH.toFixed(2);
    this.pwcfVal();
  }

  pwcfVal(){
    var prefinal = Number(this.RPH) * 200 / 100;
    var postfinal = Number(prefinal) * 2.167;
    var subfinal = Number(postfinal) * Number(this.EHC);
    var total = Number(subfinal) * Number(this.PWCF);
    this.finalbid = "$ " + total.toFixed(2);
  }

  onSubmit() {
    const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.account.trim() == "") {
      this.presentToast("Please enter Account Details.");
    } else if (this.email.trim() == "") {
      this.presentToast("Please enter Email.");
    } else if (!emailPattern.test(this.email)) {
      this.presentToast('Wrong Email Format...');
    } else {
      this.showLoader('Please wait...');     
      this.storage.get('userDetails').then((val) => {
        this.data = val;
        var body = {
          source: "mobile",
          user_id: this.data.user_id,
          account_name: this.account,
          carpet_area: this.CA,
          pr_carpet_area: this.PR_CA,
          estimate_carpet_area: this.ET_CA,
          hard_area: this.HA,
          pr_hard_area: this.PR_HA,
          estimate_hard_area: this.ET_HA,
          no_of_tums: this.numofTUMS,
          facility_each_service: this.EHC,
          per_worker_per_hour: this.WPH,
          hourly_charge_rate: this.RPH,
          clean_frequency: this.PWCF,
          bid_estimate: this.finalbid,
          email: this.email
        };
        this.authService.postData("api/commercial/mail", body).then(result => {
          this.data = result;
          console.log(this.data);
          this.hideLoader();
          if (this.data.success == true) {            
            this.popup = true;
          } else {
            this.presentToast(this.data.message);
          }
        },
          error => {
            this.hideLoader();
          });
      });
    }
  }

  onContinue() {
    this.popup = false;
    this.navCtrl.navigateRoot('/home');
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
