import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SupplydetailsPage } from './supplydetails.page';

const routes: Routes = [
  {
    path: '',
    component: SupplydetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SupplydetailsPageRoutingModule {}
