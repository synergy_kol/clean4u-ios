import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Getleads3PageRoutingModule } from './getleads3-routing.module';

import { Getleads3Page } from './getleads3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Getleads3PageRoutingModule
  ],
  declarations: [Getleads3Page]
})
export class Getleads3PageModule {}
