import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-getleads',
  templateUrl: './getleads.page.html',
  styleUrls: ['./getleads.page.scss'],
})
export class GetleadsPage implements OnInit {

  company: any = '';
  phone: any = '';
  altphone: any = '';
  email: any = '';

  data: any = [];

  isDisabled: boolean = false;
  isLoading = false;

  pageContent: any = "";

  constructor(
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private storage: Storage,
    private authService: AuthServiceProvider
  ) { }

  onKeyboard(event) {
    if (event != null) {
      event.setFocus();
    } else {
      this.onNext();
    }
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('getLeadDetails').then((val) => {
      this.hideLoader();
      this.data = val;
      this.company = this.data.company;
      this.phone = this.data.phone;
      this.altphone = this.data.altphone;
      this.email = this.data.email;      
    });
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      console.log(this.data);
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        slug: "request-lead"
      };      
      this.authService.postData("api/lead/cms/pages", body).then(result => {
        this.data = result;
        console.log("Contant: ", this.data);
        this.hideLoader();
        this.pageContent = this.data.data;
      },
        error => {
          this.hideLoader();
        });
    });
  }

  ngOnInit() {
  }

  onNext(){
    const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const phonePattern = /[^0-9]/;
    const namePattern = /[^a-z A-Z]/;
    if (this.company.trim() == '') {
      this.presentToast('Please enter Company Name');
    } else if (namePattern.test(this.company)) {
      this.presentToast('Company Name field should be Only Letters...');
    } else if (this.phone.trim() == '') {
      this.presentToast('Please enter Phone Number');
    } else if (phonePattern.test(this.phone)) {
      this.presentToast('Phone field should be Only Numbers...');
    } else if (this.phone.length <= 9) {
      this.presentToast('Phone! Please enter minimum 10 Numbers');
    } else if (this.altphone.trim() == '') {
      this.presentToast('Please enter Alternative Phone Number');
    } else if (phonePattern.test(this.altphone)) {
      this.presentToast('Alternative Phone field should be Only Numbers...');
    } else if (this.altphone.length <= 9) {
      this.presentToast('Alternative Phone! Please enter minimum 10 Numbers');
    } else if (this.email.trim() == '') {
      this.presentToast('Please enter your Email ID');
    } else if (!emailPattern.test(this.email)) {
      this.presentToast('Wrong Email Format...');
    } else {
      this.showLoader('Please wait...');
      var leadBody = {
        company: this.company,
        phone: this.phone,
        altphone: this.altphone,
        email: this.email     
      }     
      setTimeout(() => {
        this.hideLoader();
        this.presentToast("Successfully Proceeded..");
        this.storage.set("getLeadDetails", leadBody);
        this.navCtrl.navigateForward("/getleads1");
       }, 3000); 
      
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
