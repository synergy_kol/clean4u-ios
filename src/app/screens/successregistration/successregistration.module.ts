import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuccessregistrationPageRoutingModule } from './successregistration-routing.module';

import { SuccessregistrationPage } from './successregistration.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuccessregistrationPageRoutingModule
  ],
  declarations: [SuccessregistrationPage]
})
export class SuccessregistrationPageModule {}
