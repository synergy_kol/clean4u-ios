import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-leads',
  templateUrl: './leads.page.html',
  styleUrls: ['./leads.page.scss'],
})
export class LeadsPage implements OnInit {

  data: any = [];
  isLoading = false;
  allLeads: any = [];

  constructor(
    private loadingController: LoadingController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id
      };
      this.authService.postData("Services/LeadList", body).then(result => {
        this.data = result;
        this.hideLoader();
        if(this.data.success == true){
          this.allLeads = this.data.data;
          console.log("Leads:", this.allLeads);
        } else {
          this.navCtrl.navigateRoot("/getleads");
        }
      },
        error => {
          this.hideLoader();
        });
    });
  }

  addNew(){
    this.navCtrl.navigateForward("/getleads");
  }

  goDetails(id){
    var object = {
      leadId: id
    }
    this.navCtrl.navigateForward(["/leaddetails"], { queryParams: object });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
