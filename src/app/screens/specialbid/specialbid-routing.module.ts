import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SpecialbidPage } from './specialbid.page';

const routes: Routes = [
  {
    path: '',
    component: SpecialbidPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SpecialbidPageRoutingModule {}
