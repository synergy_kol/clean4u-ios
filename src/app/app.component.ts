import { Component } from '@angular/core';

import { Platform, NavController, ToastController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Router } from '@angular/router';
import { NetworkServiceProvider } from 'src/auth-providers/network-service/network-service';
import { debounceTime } from 'rxjs/operators';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { SettingsService } from './settings.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  counter = 0;
  data: any = [];
  isConnected: any;
  cityList: any = [];
  selectedTheme: String;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navCtrl: NavController,
    private storage: Storage,
    private screenOrientation: ScreenOrientation,
    private router: Router,
    private networkservice: NetworkServiceProvider,
    private toastController: ToastController,
    private settings: SettingsService
  ) {
    // Use matchMedia to check the user preference
    const prefersDark = window.matchMedia('(prefers-color-scheme: light)');

    if (prefersDark.matches === false) {
      this.settings.setActiveTheme('light-theme');
    } else {
      this.settings.setActiveTheme('dark-theme');
    }
    this.settings.getActiveTheme().subscribe(val => this.selectedTheme = val);

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('#ffffff');
      this.splashScreen.hide();

      this.storage.get('userDetails').then((val) => {
        console.log('User Details', val);
        if (val != null) {
          this.data = val;
          this.storage.get('UseFor').then((newval) => {
            if (newval != null) {
              this.navCtrl.navigateRoot('/home');
            } else {
              this.navCtrl.navigateRoot('/welcome');
            }
          });
        } else {
          this.navCtrl.navigateRoot('/login');
        }
      });

      this.platform.backButton.subscribe(() => {
        console.log("URL: ", this.router.url);
        if (this.router.url === '/login' || this.router.url === '/welcome' || this.router.url === '/home' || this.router.url === '/buykits' || this.router.url === '/settings' || this.router.url === '/myprofile') {
          if (this.counter == 0) {
            this.counter++;
            this.presentToast('Press again to exit');
            setTimeout(() => { this.counter = 0 }, 3000);
          } else {
            navigator['app'].exitApp();
          }
        }
      });

    });
  }

  networkSubscriber(): void {
    this.networkservice.getNetworkStatus().pipe(debounceTime(300))
      .subscribe((connected: boolean) => {
        this.isConnected = connected;
        if (this.isConnected == true) {
          console.log('Connected', this.isConnected);
        } else {
          this.presentToast("SORRY!, You don't have any Network Connection.");
        }
      });
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }
}
