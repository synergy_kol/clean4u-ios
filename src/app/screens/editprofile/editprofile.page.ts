import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.page.html',
  styleUrls: ['./editprofile.page.scss'],
})
export class EditprofilePage implements OnInit {

  fname: any = '';
  lname: any = '';
  phone: any = '';
  address: any = '';
  about: any = '';

  data: any = [];
  userData: any = [];
  userId: any = "";

  isDisabled: boolean = false;
  isLoading = false;

  constructor(
    private storage: Storage,
    private authService: AuthServiceProvider,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      this.userId = this.data.user_id;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
      };
      this.authService.postData("Services/ProfileDetails", body).then(result => {
        this.data = result;
        console.log("profile: ", this.data);
        this.userData = this.data.data;
        this.fname = this.userData.first_name;
        this.lname = this.userData.last_name;
        this.phone = this.userData.phone_number;
        this.address = this.userData.address;
        this.about = this.userData.about_us;
        this.hideLoader();       
      },
        error => {
          this.hideLoader();
        });
    });
  }

  onSaveProfile(){
    const phonePattern = /[^0-9]/;
    const namePattern = /[^a-z A-Z]/;
    if (this.fname.trim() == '') {
      this.presentToast('Please enter your First Name');
    } else if (namePattern.test(this.fname)) {
      this.presentToast('First Name field should be Only Letters...');
    } else if (this.lname.trim() == '') {
      this.presentToast('Please enter your Last Name');
    } else if (namePattern.test(this.lname)) {
      this.presentToast('Last Name field should be Only Letters...');
    } else if (this.phone.trim() == '') {
      this.presentToast('Please enter Phone Number');
    } else if (phonePattern.test(this.phone)) {
      this.presentToast('Phone field should be Only Numbers...');
    } else if (this.phone.length <= 9) {
      this.presentToast('Phone! Please enter minimum 10 Numbers');
    } else {
      this.showLoader('Please wait...');
      var body = {
        user_id: this.userId,
        source: "mobile",
        first_name: this.fname,
        last_name: this.lname,
        phone_number: this.phone,
        address: this.address,
        about_us: this.about 
      }
      this.authService.postData("Services/UpdateProfile", body).then(result => {
        this.hideLoader();
        this.data = result;
        console.log(this.data);
        if (this.data.success == true) {          
          this.presentToast("Your profile has been Updated Successfully.");
          this.navCtrl.navigateRoot('/settings');
        } else {
          this.presentToast(this.data.status.message);
        }
      },
        error => {
          this.hideLoader();
        });
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
