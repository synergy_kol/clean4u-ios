import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GetleadsPageRoutingModule } from './getleads-routing.module';

import { GetleadsPage } from './getleads.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GetleadsPageRoutingModule
  ],
  declarations: [GetleadsPage]
})
export class GetleadsPageModule {}
