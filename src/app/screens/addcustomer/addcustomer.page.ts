import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Camera } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
import { ActionSheetController, LoadingController, NavController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';
declare var cordova: any;

@Component({
  selector: 'app-addcustomer',
  templateUrl: './addcustomer.page.html',
  styleUrls: ['./addcustomer.page.scss'],
})
export class AddcustomerPage implements OnInit {

  data: any = [];
  userData: any = [];
  newdata: any = [];
  lastImage: string = null;

  profilePic: any = "";
  fname: any = "";
  lname: any = "";
  phone: any = "";
  email: any = "";
  address: any = "";

  isDisabled: boolean = false;
  isLoading = false;

  customerId: any;
  invoiceId: any;

  constructor(
    private loadingController: LoadingController,
    private toastController: ToastController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private actionSheet: ActionSheetController,
    private camera: Camera,
    private platform: Platform,
    private filePath: FilePath,
    private file: File,
    private navCtrl: NavController,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.queryParams.subscribe((res) => {
      this.customerId = res.custId;
      this.invoiceId = res.invId;
    });
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    if (this.customerId != null && this.invoiceId != null) {
      this.storage.get('userDetails').then((val) => {
        this.data = val;
        console.log(this.data);
        var body = {
          source: "mobile",
          user_id: this.data.user_id,
          customer_id: this.customerId,
          invoice_id: this.invoiceId
        };
        this.authService.postData("invoice/customer/details", body).then(result => {
          this.data = result;
          this.userData = this.data.data;
          console.log("Customer: ", this.userData);
          this.hideLoader();
          this.fname = this.userData.first_name;
          this.lname = this.userData.last_name;
          this.phone = this.userData.phone_number;
          this.email = this.userData.email_address;
          this.address = this.userData.address;
          this.profilePic = this.userData.profile_image;
        },
          error => {
            this.hideLoader();
          });
      });
    } else {
      this.hideLoader();
    }
  }

  onKeyboard(event) {
    if (event != null) {
      event.setFocus();
    } else {
      this.onSave();
    }
  }

  ngOnInit() {
  }

  uploadPP() {
    let actionSheet = this.actionSheet.create({
      header: 'Select Image Source',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takePP(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      }, {
        text: 'Use Camera',
        handler: () => {
          this.takePP(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    }).then(a => {
      a.present();
    });
  }
  takePP(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            console.log("filePath: ", filePath);
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      console.log(err);
    });
  }
  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }
  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      this.saveProfilePic(this.lastImage);
    }, error => {
      console.log("error", error);
    });
  }
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }
  saveProfilePic(pic) {
    this.showLoader('Uploading...');
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var apiFunc = 'invoice/customer/profileImage';
      var targetPath = this.pathForImage(pic);
      var options: any;
      var filename = pic;
      options = {
        fileKey: "profile_image",
        fileName: filename,
        chunkedMode: false,
        mimeType: "image/jpeg", //"multipart/form-data",
        params: {
          fileKey: filename,
          user_id: this.data.user_id,
          customer_id: this.customerId
        }
      };
      console.log("options: ", options);
      this.authService.fileUpload(targetPath, apiFunc, options).then(result => {
        this.newdata = result;
        console.log("Your data: ", this.newdata);
        this.presentToast("Profile Picture Uploaded Successfuly.");
        this.getDetails();
      },
        error => {
          console.log(error);
          this.hideLoader();
        });
    });
  }

  onSave() {
    const phonePattern = /[^0-9]/;
    const namePattern = /[^a-z A-Z]/;
    const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.fname.trim() == '') {
      this.presentToast('Please enter your First Name');
    } else if (namePattern.test(this.fname)) {
      this.presentToast('First Name field should be Only Letters...');
    } else if (this.lname.trim() == '') {
      this.presentToast('Please enter your Last Name');
    } else if (namePattern.test(this.lname)) {
      this.presentToast('Last Name field should be Only Letters...');
    } else if (this.phone.trim() == '') {
      this.presentToast('Please enter Phone Number');
    } else if (phonePattern.test(this.phone)) {
      this.presentToast('Phone field should be Only Numbers...');
    } else if (this.phone.length <= 9) {
      this.presentToast('Phone! Please enter minimum 10 Numbers');
    } else if (this.email.trim() == '') {
      this.presentToast('Please enter your Email ID');
    } else if (!emailPattern.test(this.email)) {
      this.presentToast('Wrong Email Format...');
    } else if (this.address.trim() == '') {
      this.presentToast('Please enter Address');
    } else {
      this.showLoader('Please wait...');
      this.storage.get('userDetails').then((val) => {
        this.data = val;
        var body = {
          user_id: this.data.user_id,
          source: "mobile",
          first_name: this.fname,
          last_name: this.lname,
          phone_number: this.phone,
          email_address: this.email,
          address: this.address,
          customer_id: this.customerId,
          invoice_id: this.invoiceId
        }
        this.authService.postData("invoice/customer/add", body).then(result => {
          this.hideLoader();
          this.data = result;
          console.log(this.data);
          if (this.data.success == true) {
            if (this.data.data == null) {
              this.presentToast("Customer Saved Successfully.");
              this.navCtrl.back();
            } else {
              this.presentToast("Customer Added Successfully.");
              var object = {
                invId: this.data.data.invoice_id
              }
              this.navCtrl.navigateRoot(["/additem"], { queryParams: object });
            }
          } else {
            this.presentToast(this.data.message);
          }
        },
          error => {
            this.hideLoader();
          });
      });
    }
  }

  /* if(this.customerId != null && this.invoiceId != null){
        
  } else {
    
  } */

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
