import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InvoicesettingsPageRoutingModule } from './invoicesettings-routing.module';

import { InvoicesettingsPage } from './invoicesettings.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InvoicesettingsPageRoutingModule
  ],
  declarations: [InvoicesettingsPage]
})
export class InvoicesettingsPageModule {}
