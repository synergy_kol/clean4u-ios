import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Getleads1Page } from './getleads1.page';

const routes: Routes = [
  {
    path: '',
    component: Getleads1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Getleads1PageRoutingModule {}
