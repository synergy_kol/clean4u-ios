import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GetleadsPage } from './getleads.page';

const routes: Routes = [
  {
    path: '',
    component: GetleadsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GetleadsPageRoutingModule {}
