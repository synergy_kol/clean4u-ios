import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-privacypolicy',
  templateUrl: './privacypolicy.page.html',
  styleUrls: ['./privacypolicy.page.scss'],
})
export class PrivacypolicyPage implements OnInit {

  data: any = [];
  pageData: any = [];
  isLoading = false;

  constructor(
    private authService: AuthServiceProvider,
    private loadingController: LoadingController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    var body = {
      slug: "Privacy-policy"
    };
    this.authService.postData("Services/GetPage", body).then(result => {
      this.data = result;
      console.log(this.data);
      this.pageData = this.data.data;
      this.hideLoader();
    },
      error => {
        this.hideLoader();
      });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
