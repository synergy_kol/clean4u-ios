import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-supplydetails',
  templateUrl: './supplydetails.page.html',
  styleUrls: ['./supplydetails.page.scss'],
})
export class SupplydetailsPage implements OnInit {

  data: any = [];
  productdetails: any = "";
  isLoading = false;
  productId: any = "";
  prodQnty = 1;
  isDisabled: boolean = false;
  incart: any = "";

  constructor(
    private loadingController: LoadingController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private navCtrl: NavController,
    private activatedRoute: ActivatedRoute,
    private toastController: ToastController
  ) {
    this.activatedRoute.queryParams.subscribe((res) => {
      this.productId = res.itemId;
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        product_id: this.productId
      };
      this.authService.postData("api/product/details", body).then(result => {
        this.data = result;
        this.productdetails = this.data.data.product_details;
        if(this.productdetails.cart_quantity != 0){
          this.prodQnty = this.productdetails.cart_quantity;
        }        
        console.log("Products Details: ", this.productdetails);
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });

      var cartqnt = {
        source: "mobile",
        user_id: this.data.user_id
      };
      this.authService.postData("api/product/cartcount", cartqnt).then(result => {
        this.data = result;
        this.incart = this.data.data;
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  lessQnty() {
    this.prodQnty--;
    if (this.prodQnty < 1) {
      this.presentToast("Sorry! Cant use less than 1");
      this.prodQnty = 1;      
    } else {
      this.onAddtocart();
    }
  }
  addQnty() {
    if (Number(this.prodQnty) >= Number(this.productdetails.in_stock)) {
      this.presentToast("Limited Stock! Last" + " " + this.productdetails.in_stock + " " + "item left!");
    } else {
      this.prodQnty++;
      this.onAddtocart();
    }
  }

  onAddtocart() {
    this.showLoader('Please wait...');
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        product_id: this.productId,
        quantity: this.prodQnty
      };
      this.authService.postData("api/product/addcart", body).then(result => {
        this.data = result;
        console.log("cart: ", this.data);
        this.hideLoader();
        if (this.data.success == true) {
          this.presentToast(this.data.message);
          this.getDetails();
        } else {
          this.presentToast(this.data.message);
        }
      },
        error => {
          this.hideLoader();
        });
    });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
