import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Getleads2PageRoutingModule } from './getleads2-routing.module';

import { Getleads2Page } from './getleads2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Getleads2PageRoutingModule
  ],
  declarations: [Getleads2Page]
})
export class Getleads2PageModule {}
