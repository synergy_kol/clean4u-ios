import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SupplydetailsPageRoutingModule } from './supplydetails-routing.module';

import { SupplydetailsPage } from './supplydetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SupplydetailsPageRoutingModule
  ],
  declarations: [SupplydetailsPage]
})
export class SupplydetailsPageModule {}
