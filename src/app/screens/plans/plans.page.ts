import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.page.html',
  styleUrls: ['./plans.page.scss'],
})
export class PlansPage implements OnInit {

  data: any = [];
  isLoading = false;
  allPlans: any = [];
  isDisabled: boolean = false;

  constructor(
    private loadingController: LoadingController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private navCtrl: NavController,
    private toastController: ToastController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id
      };
      this.authService.postData("api/plan/list", body).then(result => {
        this.data = result;
        this.hideLoader();
        this.allPlans = this.data.data;
        console.log("Plans:", this.allPlans);
      },
        error => {
          this.hideLoader();
        });
    });
  }

  onPay(id, price, issubscribed){
    if(issubscribed == 0){
      var object = {
        subid: id,
        amount: price
      }
      this.navCtrl.navigateForward(["/planspayment"], { queryParams: object });
    } else {
      this.presentToast("Sorry! This Plan Currently Activated.");
    }
    
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
