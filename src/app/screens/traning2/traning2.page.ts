import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-traning2',
  templateUrl: './traning2.page.html',
  styleUrls: ['./traning2.page.scss'],
})
export class Traning2Page implements OnInit {

  data: any = [];
  audioData: any = [];
  isLoading = false;

  track: any = "";
  trackID: any = "";
  trackTitle: any = "";

  constructor(
    private authService: AuthServiceProvider,
    private loadingController: LoadingController,
    private storage: Storage,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
      };
      this.authService.postData("Services/AudioList", body).then(result => {
        this.data = result;
        console.log(this.data);
        this.audioData = this.data.data;
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });    
  }

  playAudio(audio, id, title){
    this.track = audio;
    this.trackID = id;
    this.trackTitle = title;
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
