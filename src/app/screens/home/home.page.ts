import { Component, OnInit } from '@angular/core';
import { SettingsService } from 'src/app/settings.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  pop = false;
  selectedTheme: String;

  constructor(private settings: SettingsService) {
    //this.settings.getActiveTheme().subscribe(val => this.selectedTheme = val);
  }
 
  toggleAppTheme(event) {
    if(event.detail.checked){
      this.settings.setActiveTheme('dark-theme');
    } else{
      this.settings.setActiveTheme('light-theme');
    }
  }

  /*toggleAppTheme(event){
    if(event.detail.checked){
      document.body.setAttribute('color-theme', 'dark');
    }
    else{
      document.body.setAttribute('color-theme', 'light');
    }
  }

  colorTest(systemInitiatedDark) {
    if (systemInitiatedDark.matches) {
      document.body.setAttribute('data-theme', 'dark');		
    } else {
      document.body.setAttribute('data-theme', 'light');
    }
  } */

  ngOnInit() {
  }

}
