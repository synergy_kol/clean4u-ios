import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-buykits',
  templateUrl: './buykits.page.html',
  styleUrls: ['./buykits.page.scss'],
})
export class BuykitsPage implements OnInit {

  data: any = [];
  allproductList: any = [];
  isLoading = false;
  pageNum: any = 10;
  isDisabled: boolean = false;
  paginatorNo: any = "";

  constructor(
    private loadingController: LoadingController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private navCtrl: NavController,
    private toastController: ToastController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        limit: this.pageNum,
        type: "combo"
      };
      this.authService.postData("api/product/list", body).then(result => {
        this.data = result;
        console.log("Products: ", this.data);
        this.allproductList = this.data.data.data;
        this.paginatorNo = this.data.data.paginator.total_rows;
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  doInfinite(event) {
    this.showLoader('Loading...');
    setTimeout(() => {
      this.pageNum += 10;
      this.getDetails();
      event.target.complete();
      console.log(event);
    }, 500);
  }

  onAddtocart(proId) {
    this.showLoader('Please wait...');
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        product_id: proId,
        quantity: 1
      };
      this.authService.postData("api/product/addcart", body).then(result => {
        this.data = result;
        console.log("cart: ", this.data);
        this.hideLoader();
        if (this.data.success == true) {
          this.presentToast("Product has been Successfully Added to Cart.");
          this.navCtrl.navigateForward("/cart");
        } else {
          this.presentToast(this.data.message);
        }
      },
        error => {
          this.hideLoader();
        });
    });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
