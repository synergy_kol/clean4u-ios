import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Traning1Page } from './traning1.page';

const routes: Routes = [
  {
    path: '',
    component: Traning1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Traning1PageRoutingModule {}
