import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Getleads1PageRoutingModule } from './getleads1-routing.module';

import { Getleads1Page } from './getleads1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Getleads1PageRoutingModule
  ],
  declarations: [Getleads1Page]
})
export class Getleads1PageModule {}
