import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BillingaddressPageRoutingModule } from './billingaddress-routing.module';

import { BillingaddressPage } from './billingaddress.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BillingaddressPageRoutingModule
  ],
  declarations: [BillingaddressPage]
})
export class BillingaddressPageModule {}
