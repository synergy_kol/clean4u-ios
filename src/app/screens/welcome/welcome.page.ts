import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  constructor(
    private navCtrl: NavController,
    private storage: Storage
  ) { }

  ngOnInit() {
  }

  onFree(){
    this.storage.set("UseFor", "Free");
    this.navCtrl.navigateRoot('/home'); 
  }

  onPlan(){
    this.navCtrl.navigateForward('/plans');
  }

}
