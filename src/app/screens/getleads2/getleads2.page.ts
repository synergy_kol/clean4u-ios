import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-getleads2',
  templateUrl: './getleads2.page.html',
  styleUrls: ['./getleads2.page.scss'],
})
export class Getleads2Page implements OnInit {

  data: any = [];
  dataLead1: any = [];
  dataLead2: any = [];
  stateList: any = [];

  isDisabled: boolean = false;
  isLoading = false;

  constructor(
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private storage: Storage,
    private alertController: AlertController,
    private authService: AuthServiceProvider
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
    this.getState();
  }

  getDetails() {
    this.storage.get('getLeadDetails').then((val1) => {
      this.hideLoader();
      this.dataLead1 = val1;
    });
    this.storage.get('getLeadDetails1').then((val2) => {
      this.hideLoader();
      this.dataLead2 = val2;
      console.log(this.dataLead2);
    });
  }

  getState(){
    var StateBody = {
      source: "mobile"
    };
    this.authService.postData("Services/StateList", StateBody).then(result => {
      this.data = result;
      console.log("State: ", this.data);
      this.stateList = this.data.data;
      this.hideLoader();
    },
      error => {
        this.hideLoader();
      });
  }

  onNext() {
    const alert = this.alertController.create({
      header: 'Are You Sure?',
      message: 'Want to Submit',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Please wait...');
            setTimeout(() => {
              this.onSubmit();
            }, 1000);
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }

  onSubmit() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        company_name: this.dataLead1.company,
        phone_number: this.dataLead1.phone,
        alternative_phone_number: this.dataLead1.altphone,
        secondary_email: this.dataLead1.email,
        state: this.dataLead2.state,
        city_data: this.dataLead2.cities,
        available_weekend_appointment: this.dataLead2.weekend,
        time_appointment: this.dataLead2.time,
        apartment_appointment: this.dataLead2.moveout,
        hotel_appointment: this.dataLead2.hotel,
        notes: this.dataLead2.note,
        address: this.dataLead2.address,
        sub_total: this.dataLead2.subtotal,
        discount: this.dataLead2.discount,
        total: this.dataLead2.total,
        coupon_code: this.dataLead2.cupon
      };
      this.authService.postData("Services/AddLeads", body).then(result => {
        this.data = result;
        this.hideLoader();
        if (this.data.success == true) {
          this.storage.remove("getLeadDetails");
          this.storage.remove("getLeadDetails1");
            var object = {
              leadid: this.data.data,
              amount: this.dataLead2.total
            }
            this.navCtrl.navigateRoot(["/getleads3"], { queryParams: object });
        } else {
          this.presentToast(this.data.message);
        }
      },
        error => {
          this.hideLoader();
        });
    });    
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
