import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-additem',
  templateUrl: './additem.page.html',
  styleUrls: ['./additem.page.scss'],
})
export class AdditemPage implements OnInit {

  data: any = [];
  userData: any = [];

  name: any = "";
  description: any = "";
  itemQnty = 1;
  amount: any = "";

  isDisabled: boolean = false;
  isLoading = false;

  invoiceId: any;
  itemId: any = null;

  constructor(
    private loadingController: LoadingController,
    private toastController: ToastController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController
  ) {
    this.activatedRoute.queryParams.subscribe((res) => {
      this.invoiceId = res.invId;
      this.itemId = res.itemId;
    });
   }

  onKeyboard(event) {
    if (event != null) {
      event.setFocus();
    } else {
      this.onAddItem();
    }
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();  
  }

  getDetails() {    
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      console.log(this.data);
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        item_id: this.itemId
      };
      this.authService.postData("invoice/item/details", body).then(result => {
        this.data = result;
        this.userData = this.data.data;
        console.log("Item Details: ", this.userData);
        this.hideLoader();
        if(this.userData != null){
          this.name = this.userData.name;
          this.description = this.userData.description;
          this.itemQnty = this.userData.quantity;
          this.amount = this.userData.amount;
        }        
      },
        error => {
          this.hideLoader();
        });
    });
  }

  ngOnInit() {
  }

  lessQnty(){
    this.itemQnty--;
    if(this.itemQnty < 1){
      this.presentToast("Sorry! Cant use less than 1");
      this.itemQnty = 1;
    }
  }
  addQnty(){
    this.itemQnty++;
  }

  onAddItem(){
    if (this.name.trim() == '') {
      this.presentToast('Please enter Item Name');
    } else if (this.description.trim() == '') {
      this.presentToast('Please enter Item Description');
    } else if (this.amount.trim() == '') {
      this.presentToast('Please enter Item Amount');
    } else {
      this.showLoader('Please wait...');
      this.storage.get('userDetails').then((val) => {
        this.data = val;
        var body = {
          source: "mobile",
          user_id: this.data.user_id,
          name: this.name,
          quantity: this.itemQnty,
          amount: this.amount,
          description: this.description,
          invoice_id: this.invoiceId,
          item_id: this.itemId
        };
        this.authService.postData("invoice/item/add", body).then(result => {
          this.data = result;
          this.hideLoader();
          if (this.data.success == true) {
            if (this.itemId != null) {
              this.presentToast("Item Saved Successfully.");
            } else {
              this.presentToast("Item Added Successfully.");
            }
            this.name = "";
            this.itemQnty = 1;
            this.amount = "";
            this.description= "";
            var object = {
              invId: this.invoiceId
            }
            this.navCtrl.navigateRoot(["/createinvoice"], { queryParams: object });
          } else {
            this.presentToast(this.data.message);
          }
        },
          error => {
            this.hideLoader();
          });
      }); 
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
