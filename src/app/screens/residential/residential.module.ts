import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResidentialPageRoutingModule } from './residential-routing.module';

import { ResidentialPage } from './residential.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResidentialPageRoutingModule
  ],
  declarations: [ResidentialPage]
})
export class ResidentialPageModule {}
