import { Component, OnInit } from '@angular/core';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-example',
  templateUrl: './example.page.html',
  styleUrls: ['./example.page.scss'],
})
export class ExamplePage implements OnInit {

  data: any = [];
  videoData: any = [];
  isLoading = false;

  constructor(
    private authService: AuthServiceProvider,
    private loadingController: LoadingController,
    private storage: Storage,
    private iab: InAppBrowser,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
      };
      this.authService.postData("Services/VideoList", body).then(result => {
        this.data = result;
        console.log(this.data);
        this.videoData = this.data.data;
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });    
  }

  playVideo(videolink) {
    const options: InAppBrowserOptions = {
      zoom: 'no',
      location: 'no',
      /* toolbar: 'no', */
      closebuttoncaption: 'Close',
    };
    const browser = this.iab.create(videolink, 'system', options);
    browser.on('loadstart').subscribe(event => {
      console.log("url: ", event.url);
    });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }
}
