import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Traning2PageRoutingModule } from './traning2-routing.module';

import { Traning2Page } from './traning2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Traning2PageRoutingModule
  ],
  declarations: [Traning2Page]
})
export class Traning2PageModule {}
