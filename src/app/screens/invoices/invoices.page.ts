import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.page.html',
  styleUrls: ['./invoices.page.scss'],
})
export class InvoicesPage implements OnInit {

  data: any = [];
  invoiceData: any = [];
  invoiceType: any = "Unpaid";

  paidActive: boolean = false;
  unpaidActive: boolean = true;

  isLoading = false;
  isDisabled: boolean = false;
  popup = false;

  cusType: any = "";
  showList: boolean = false;
  customerList: any = [];
  existcustomer: any = "";

  constructor(
    private loadingController: LoadingController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private navCtrl: NavController,
    private toastController: ToastController
  ) { }

  ngOnInit() {
  }

  customPopoverOptions: any = {
    header: 'Please Select Customer'
  };

  oninvoiceType(type) {
    this.invoiceType = type;
    this.showLoader('Please wait...');
    this.getDetails();
    if (type == "Paid") {
      this.paidActive = true;
      this.unpaidActive = false;
    } else {
      this.paidActive = false;
      this.unpaidActive = true;
    }
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      console.log(this.data);
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        invocie_status: this.invoiceType
      };
      this.authService.postData("invoice/all/list", body).then(result => {
        this.data = result;
        console.log("Invoice: ", this.data);
        this.hideLoader();
        this.invoiceData = this.data.data;
      },
        error => {
          this.hideLoader();
        });
      var clientbody = {
        source: "mobile",
        user_id: this.data.user_id,
      };
      this.authService.postData("invoice/user/clientlist", clientbody).then(result => {
        this.data = result;
        console.log("Customer: ", this.data);
        this.hideLoader();
        this.customerList = this.data.data;
      },
        error => {
          this.hideLoader();
        });
    });
  }

  goInvoice(id) {
    var object = {
      invId: id
    }
    this.navCtrl.navigateForward(["/createinvoice"], { queryParams: object });
  }

  createInvoice() {
    if (this.customerList == null) {
      this.navCtrl.navigateRoot("/addcustomer");
    } else {
      this.popup = true;
    }
  }

  onType() {
    if (this.cusType == "existing") {
      this.showList = true;
      /* this.storage.get('userDetails').then((val) => {
        this.data = val;
        console.log(this.data);
        var body = {
          source: "mobile",
          user_id: this.data.user_id,
        };
        this.authService.postData("invoice/user/clientlist", body).then(result => {
          this.data = result;
          console.log("Customer: ", this.data);
          this.hideLoader();
          this.customerList = this.data.data;
        },
          error => {
            this.hideLoader();
          });
      }); */
    } else {
      this.popup = false;
      this.navCtrl.navigateRoot("/addcustomer");
    }
  }


  onexistCust() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        customer_id: this.existcustomer
      };
      this.authService.postData("customerbyid/create/invoice", body).then(result => {
        this.data = result;
        console.log("Customer Exist: ", this.data);
        this.hideLoader();
        this.popup = false;
        if (this.data.success == true) {
          this.presentToast("Customer Added Successfully.");
          var object = {
            invId: this.data.data.invoice_id
          }
          this.navCtrl.navigateForward(["/additem"], { queryParams: object });
        } else {
          this.presentToast(this.data.message);
        }
      },
        error => {
          this.hideLoader();
        });
    });
  }

  onClose() {
    this.popup = false;
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
