import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { Network } from '@ionic-native/network/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { PreviewAnyFile } from '@ionic-native/preview-any-file/ngx';
import { PayPal } from '@ionic-native/paypal/ngx';
import { Stripe } from '@ionic-native/stripe/ngx';
import { ApplePay } from '@ionic-native/apple-pay/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./screens/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./screens/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('./screens/welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: 'plans',
    loadChildren: () => import('./screens/plans/plans.module').then( m => m.PlansPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./screens/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'invoices',
    loadChildren: () => import('./screens/invoices/invoices.module').then( m => m.InvoicesPageModule)
  },
  {
    path: 'getleads',
    loadChildren: () => import('./screens/getleads/getleads.module').then( m => m.GetleadsPageModule)
  },
  {
    path: 'traning',
    loadChildren: () => import('./screens/traning/traning.module').then( m => m.TraningPageModule)
  },
  {
    path: 'calculators',
    loadChildren: () => import('./screens/calculators/calculators.module').then( m => m.CalculatorsPageModule)
  },
  {
    path: 'supplies',
    loadChildren: () => import('./screens/supplies/supplies.module').then( m => m.SuppliesPageModule)
  },
  {
    path: 'help',
    loadChildren: () => import('./screens/help/help.module').then( m => m.HelpPageModule)
  },
  {
    path: 'commercial',
    loadChildren: () => import('./screens/commercial/commercial.module').then( m => m.CommercialPageModule)
  },
  {
    path: 'example',
    loadChildren: () => import('./screens/example/example.module').then( m => m.ExamplePageModule)
  },
  {
    path: 'residential',
    loadChildren: () => import('./screens/residential/residential.module').then( m => m.ResidentialPageModule)
  },
  {
    path: 'specialbid',
    loadChildren: () => import('./screens/specialbid/specialbid.module').then( m => m.SpecialbidPageModule)
  },
  {
    path: 'buykits',
    loadChildren: () => import('./screens/buykits/buykits.module').then( m => m.BuykitsPageModule)
  },
  {
    path: 'traning1',
    loadChildren: () => import('./screens/traning1/traning1.module').then( m => m.Traning1PageModule)
  },
  {
    path: 'traning2',
    loadChildren: () => import('./screens/traning2/traning2.module').then( m => m.Traning2PageModule)
  },
  {
    path: 'createinvoice',
    loadChildren: () => import('./screens/createinvoice/createinvoice.module').then( m => m.CreateinvoicePageModule)
  },
  {
    path: 'addcustomer',
    loadChildren: () => import('./screens/addcustomer/addcustomer.module').then( m => m.AddcustomerPageModule)
  },
  {
    path: 'additem',
    loadChildren: () => import('./screens/additem/additem.module').then( m => m.AdditemPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./screens/settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'myprofile',
    loadChildren: () => import('./screens/myprofile/myprofile.module').then( m => m.MyprofilePageModule)
  },
  {
    path: 'supplydetails',
    loadChildren: () => import('./screens/supplydetails/supplydetails.module').then( m => m.SupplydetailsPageModule)
  },
  {
    path: 'cart',
    loadChildren: () => import('./screens/cart/cart.module').then( m => m.CartPageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./screens/payment/payment.module').then( m => m.PaymentPageModule)
  },
  {
    path: 'success',
    loadChildren: () => import('./screens/success/success.module').then( m => m.SuccessPageModule)
  },
  {
    path: 'forgotpass',
    loadChildren: () => import('./screens/forgotpass/forgotpass.module').then( m => m.ForgotpassPageModule)
  },
  {
    path: 'termsconditions',
    loadChildren: () => import('./screens/termsconditions/termsconditions.module').then( m => m.TermsconditionsPageModule)
  },
  {
    path: 'getleads1',
    loadChildren: () => import('./screens/getleads1/getleads1.module').then( m => m.Getleads1PageModule)
  },
  {
    path: 'getleads2',
    loadChildren: () => import('./screens/getleads2/getleads2.module').then( m => m.Getleads2PageModule)
  },
  {
    path: 'getleads3',
    loadChildren: () => import('./screens/getleads3/getleads3.module').then( m => m.Getleads3PageModule)
  },
  {
    path: 'editprofile',
    loadChildren: () => import('./screens/editprofile/editprofile.module').then( m => m.EditprofilePageModule)
  },
  {
    path: 'privacypolicy',
    loadChildren: () => import('./screens/privacypolicy/privacypolicy.module').then( m => m.PrivacypolicyPageModule)
  },
  {
    path: 'successregistration',
    loadChildren: () => import('./screens/successregistration/successregistration.module').then( m => m.SuccessregistrationPageModule)
  },
  {
    path: 'invoicesettings',
    loadChildren: () => import('./screens/invoicesettings/invoicesettings.module').then( m => m.InvoicesettingsPageModule)
  },
  {
    path: 'billingaddress',
    loadChildren: () => import('./screens/billingaddress/billingaddress.module').then( m => m.BillingaddressPageModule)
  },
  {
    path: 'planspayment',
    loadChildren: () => import('./screens/planspayment/planspayment.module').then( m => m.PlanspaymentPageModule)
  },
  {
    path: 'leads',
    loadChildren: () => import('./screens/leads/leads.module').then( m => m.LeadsPageModule)
  },
  {
    path: 'leaddetails',
    loadChildren: () => import('./screens/leaddetails/leaddetails.module').then( m => m.LeaddetailsPageModule)
  },
  {
    path: 'myorder',
    loadChildren: () => import('./screens/myorder/myorder.module').then( m => m.MyorderPageModule)
  }
];

@NgModule({
  imports: [
    HttpClientModule,
    IonicStorageModule.forRoot(),
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  providers: [
    FileTransfer,
    Network,
    ScreenOrientation,
    AuthServiceProvider,
    InAppBrowser,
    PreviewAnyFile,
    PayPal,
    Stripe,
    ApplePay,
    Facebook
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
