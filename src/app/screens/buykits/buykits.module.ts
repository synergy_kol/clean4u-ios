import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuykitsPageRoutingModule } from './buykits-routing.module';

import { BuykitsPage } from './buykits.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuykitsPageRoutingModule
  ],
  declarations: [BuykitsPage]
})
export class BuykitsPageModule {}
