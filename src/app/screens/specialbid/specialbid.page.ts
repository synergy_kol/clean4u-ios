import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-specialbid',
  templateUrl: './specialbid.page.html',
  styleUrls: ['./specialbid.page.scss'],
})
export class SpecialbidPage implements OnInit {

  popup = false;
  data: any = [];
  isLoading = false;
  isDisabled: boolean = false;

  account: any = "";

  disinfectant_sqft: any = "";
  disinfectant_PE: any = 0;

  construction_sqft: any = "";
  construction_charge_per_sqft: any = "";
  construction_PE: any = 0;

  apartment_sqft: any = "";
  apartment_charge_per_sqft: any = "";
  apartment_PE: any = 0;

  strip_sqft: any = "";
  strip_charge_per_sqft: any = "";
  strip_PE: any = 0;

  scrub_sqft: any = "";
  scrub_charge_per_sqft: any = "";
  scrub_PE: any = 0;

  autoscrub_sqft: any = "";
  autoscrub_charge_per_sqft: any = "";
  autoscrub_PE: any = 0;

  no_window: any = "";
  no_window_PE: any = 0;
  window_sqft: any = "";
  window_charge_per_sqft: any = "";
  window_PE: any = 0;

  washing_sqft: any = "";
  washing_charge_per_sqft: any = "";
  washing_PE: any = 0;

  extraction_sqft: any = "";
  extraction_charge_per_sqft: any = "";
  extraction_PE: any = 0;

  bonnet_sqft: any = "";
  bonnet_charge_per_sqft: any = "";
  bonnet_PE: any = 0;

  buff_sqft: any = "";
  buff_charge_per_sqft: any = "";
  buff_PE: any = 0;

  email: any = "";

  constructor(
    private loadingController: LoadingController,
    private navCtrl: NavController,
    private toastController: ToastController,
    private authService: AuthServiceProvider,
    private storage: Storage
  ) { }

  ngOnInit() {
  }

  dsnftsqftVal() {
    var calPE = Number(this.disinfectant_sqft) / 5;
    this.disinfectant_PE = "$ " + calPE.toFixed(2);
  }

  cnstrtsqftVal() {
    var calPE = Number(this.construction_sqft) * Number(this.construction_charge_per_sqft);
    this.construction_PE = "$ " + calPE.toFixed(2);
  }

  aprtmntsqftVal() {
    var calPE = Number(this.apartment_sqft) * Number(this.apartment_charge_per_sqft);
    this.apartment_PE = "$ " + calPE.toFixed(2);
  }

  strpsqftVal() {
    var calPE = Number(this.strip_sqft) * Number(this.strip_charge_per_sqft);
    this.strip_PE = "$ " + calPE.toFixed(2);
  }

  scrbsqftVal() {
    var calPE = Number(this.scrub_sqft) * Number(this.scrub_charge_per_sqft);
    this.scrub_PE = "$ " + calPE.toFixed(2);
  }

  autoscrbsqftVal() {
    var calPE = Number(this.autoscrub_sqft) * Number(this.autoscrub_charge_per_sqft);
    this.autoscrub_PE = "$ " + calPE.toFixed(2);
  }

  windowVal() {
    var calPE = Number(this.no_window) * 7;
    this.no_window_PE = "$ " + calPE.toFixed(2);
  }

  wndwsqftVal() {
    var calPE = Number(this.window_sqft) * Number(this.window_charge_per_sqft);
    this.window_PE = "$ " + calPE.toFixed(2);
  }

  wsngsqftVal() {
    var calPE = Number(this.washing_sqft) * Number(this.washing_charge_per_sqft);
    this.washing_PE = "$ " + calPE.toFixed(2);
  }

  extrnsqftVal() {
    var calPE = Number(this.extraction_sqft) * Number(this.extraction_charge_per_sqft);
    this.extraction_PE = "$ " + calPE.toFixed(2);
  }

  bntsqftVal() {
    var calPE = Number(this.bonnet_sqft) * Number(this.bonnet_charge_per_sqft);
    this.bonnet_PE = "$ " + calPE.toFixed(2);
  }

  buffsqftVal() {
    var calPE = Number(this.buff_sqft) * Number(this.buff_charge_per_sqft);
    this.buff_PE = "$ " + calPE.toFixed(2);
  }

  onSubmit() {
    const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.account.trim() == "") {
      this.presentToast("Please enter Account Name.");
    } else if (this.email.trim() == "") {
      this.presentToast("Please enter Email.");
    } else if (!emailPattern.test(this.email)) {
      this.presentToast('Wrong Email Format...');
    } else {
      this.showLoader('Please wait...');
      this.storage.get('userDetails').then((val) => {
        this.data = val;
        var body = {
          source: "mobile",
          user_id: this.data.user_id,
          account: this.account,
          no_of_disinfection_area: this.disinfectant_sqft,
          estimate_of_disinfection_area: this.disinfectant_PE,
          no_of_post_construction_area: this.construction_sqft,
          charge_of_post_construction_area: this.construction_charge_per_sqft,
          estimate_of_post_construction_area: this.construction_PE,
          no_of_appartment_area: this.apartment_sqft,
          charge_of_appartment_area: this.apartment_charge_per_sqft,
          estimate_of_appartment_area: this.apartment_PE,
          no_of_strip_wax_area: this.strip_sqft,
          charge_of_strip_wax_area: this.strip_charge_per_sqft,
          estimate_of_strip_wax_area: this.strip_PE,
          no_of_scrub_recoat_area: this.scrub_sqft,
          charge_of_scrub_recoat_area: this.scrub_charge_per_sqft,
          estimate_of_scrub_recoat_area: this.scrub_PE,
          no_of_machine_scrub_area: this.autoscrub_sqft,
          charge_of_machine_scrub_area: this.autoscrub_charge_per_sqft,
          estimate_of_machine_scrub_area: this.autoscrub_PE,
          no_of_windows: this.no_window,
          estimate_of_windows: this.no_window_PE,
          no_of_window_cleaning_area: this.window_sqft,
          charge_of_window_cleaning_area: this.window_charge_per_sqft,
          estimate_of_window_cleaning_area: this.window_PE,
          no_of_pressure_washing_area: this.washing_sqft,
          charge_of_pressure_washing_area: this.washing_charge_per_sqft,
          estimate_of_pressure_washing_area: this.washing_PE,
          no_of_carpet_extraction_area: this.extraction_sqft,
          charge_of_carpet_extraction_area: this.extraction_charge_per_sqft,
          estimate_of_carpet_extraction_area: this.extraction_PE,
          no_of_carpet_bonnet_area: this.bonnet_sqft,
          charge_of_carpet_bonnet_area: this.bonnet_charge_per_sqft,
          estimate_of_carpet_bonnet_area: this.bonnet_PE,
          no_of_high_speed_buff_area: this.buff_sqft,
          charge_of_high_speed_buff_area: this.buff_charge_per_sqft,
          estimate_of_high_speed_buff_area: this.buff_PE,
          email: this.email
        };
        this.authService.postData("Services/SendSpecialCalculationEmail", body).then(result => {
          this.data = result;
          console.log(this.data);
          if (this.data.success == true) {
            this.hideLoader();
            this.popup = true;
          } else {
            this.presentToast(this.data.message);
          }
        },
          error => {
            this.hideLoader();
          });
      });
    }
  }

  onContinue() {
    this.popup = false;
    this.navCtrl.navigateRoot('/home');
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
