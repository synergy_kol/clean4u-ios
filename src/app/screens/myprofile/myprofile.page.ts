import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AlertController, LoadingController, NavController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';
import { Camera } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
declare var cordova: any;

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.page.html',
  styleUrls: ['./myprofile.page.scss'],
})
export class MyprofilePage implements OnInit {

  data: any = [];
  userData: any = [];
  newdata: any = [];

  profilePic: any = "";
  usersuburb: any = '';
  lastImage: string = null;

  isDisabled: boolean = false;
  isLoading = false;

  plan: any = "";

  constructor(
    private loadingController: LoadingController,
    private toastController: ToastController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private actionSheet: ActionSheetController,
    private camera: Camera,
    private platform: Platform,
    public filePath: FilePath,
    public file: File,
    private alertController: AlertController,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
      };
      this.authService.postData("Services/ProfileDetails", body).then(result => {
        this.data = result;
        console.log("profile: ", this.data);
        this.userData = this.data.data;
        this.profilePic = this.userData.profile_image;
        this.plan = this.userData.subscription_details;
        this.hideLoader();       
      },
        error => {
          this.hideLoader();
        });
    });
  }

  uploadPP() {
    let actionSheet = this.actionSheet.create({
      header: 'Select Image Source',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takePP(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      }, {
        text: 'Use Camera',
        handler: () => {
          this.takePP(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    }).then(a => {
      a.present();
    });
  }
  takePP(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            console.log("filePath: ", filePath);
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      console.log(err);
    });
  }
  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }
  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      this.saveProfilePic(this.lastImage);
    }, error => {
      console.log("error", error);
    });
  }
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }
  saveProfilePic(pic) {
    this.showLoader('Uploading...');
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var apiFunc = 'Services/UpdateProfileImage';
      var targetPath = this.pathForImage(pic);
      var options: any;
      var filename = pic;
      options = {
        fileKey: "profile_image",
        fileName: filename,
        chunkedMode: false,
        mimeType: "image/jpeg", //"multipart/form-data",
        params: {
          fileKey: filename,
          user_id: this.data.user_id
        }
      };
      console.log("options: ", options);            
      this.authService.fileUpload(targetPath, apiFunc, options).then(result => {
        this.newdata = result;        
        console.log("Your data: ", this.newdata);
        this.presentToast("Profile Picture Uploaded Successfuly.");
        this.getDetails();        
      },
        error => {
          console.log(error);
          this.hideLoader();
        });
    });
  }

  canSub(plmid){
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, you want to Cancel your Subscription?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Wait...');
            this.storage.get('userDetails').then((val) => {
              this.data = val;
              var body = {
                source: "mobile",
                user_id: this.data.user_id,
                plan_id: plmid
              };
              this.authService.postData("api/cancel/subscription", body).then(result => {
                this.data = result;
                this.userData = this.data.data;
                console.log("Subscription: ", this.data);
                this.hideLoader();
                if (this.data.success == true) {
                  this.presentToast("Subscription cancelled successfully.");
                  this.navCtrl.navigateRoot('/home');
                } else {
                  this.presentToast(this.data.message);
                }
              },
                error => {
                  this.hideLoader();
                });
            })
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
