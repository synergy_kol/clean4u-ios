import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-createinvoice',
  templateUrl: './createinvoice.page.html',
  styleUrls: ['./createinvoice.page.scss'],
})
export class CreateinvoicePage implements OnInit {

  invoiceId: any = "";
  data: any = [];
  invoiceDetails: any = [];
  companyDetails: any = [];
  itemDetails: any = [];
  discountamount: any = "";

  isLoading = false;
  isDisabled: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private loadingController: LoadingController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private navCtrl: NavController,
    private toastController: ToastController,
    private alertController: AlertController
  ) {
    this.activatedRoute.queryParams.subscribe((res) => {
      this.invoiceId = res.invId;
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  onTool(item){
    item.showMore = !item.showMore(item, 1)
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      console.log(this.data);
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        invoice_id: this.invoiceId
      };
      this.authService.postData("invoice/single/details", body).then(result => {
        this.data = result;
        console.log("Invoice Details: ", this.data);
        this.hideLoader();
        this.invoiceDetails = this.data.data;
        this.companyDetails = this.invoiceDetails.invoice_setting;
        this.itemDetails = this.invoiceDetails.invoice_items;
      },
        error => {
          this.hideLoader();
        });
    });
  }

  onEdit(cusid) {
    var object = {
      custId: cusid,
      invId: this.invoiceId
    }
    this.navCtrl.navigateForward(["/addcustomer"], { queryParams: object });
  }

  onAdd(id) {
    var object = {
      invId: id
    }
    this.navCtrl.navigateForward(["/additem"], { queryParams: object });
  }

  onPaid() {
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, you want to Pay?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Please wait...');
            setTimeout(() => {
              this.storage.get('userDetails').then((val) => {
                this.data = val;
                var body = {
                  source: "mobile",
                  user_id: this.data.user_id,
                  invoice_id: this.invoiceId,
                  invocie_status: "Paid"
                };
                this.authService.postData("invoice/update/status", body).then(result => {
                  this.data = result;
                  this.hideLoader();
                  if (this.data.success == true) {
                    this.presentToast("Your Invoice Paid Successfully.");
                    this.getDetails();
                  } else {
                    this.presentToast(this.data.message);
                  }
                },
                  error => {
                    this.hideLoader();
                  });
              });
            }, 1000);
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }

  applyDiscount() {
    if (this.discountamount.trim() == '') {
      this.presentToast('Please enter Discount');
    } else if (Number(this.discountamount) >= Number(this.invoiceDetails.total_amount)) {
      this.presentToast('Discount amount should not be equal or greater then Total Amount');
    } else {
      const alert = this.alertController.create({
        header: 'Warning!',
        message: 'Are you sure, want you add Discount?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Yes',
            handler: () => {
              this.showLoader('Please wait...');
              this.storage.get('userDetails').then((val) => {
                this.data = val;
                var body = {
                  source: "mobile",
                  user_id: this.data.user_id,
                  discount: this.discountamount,
                  invoice_id: this.invoiceId
                };
                this.authService.postData("invoice/discount/add", body).then(result => {
                  this.data = result;
                  console.log("Discount: ", this.data);
                  this.hideLoader();
                  if (this.data.success == true) {
                    this.presentToast("Discount Applied Successfully.");
                    this.getDetails();
                  } else {
                    this.presentToast("Sorry! " + this.data.message);
                  }
                },
                  error => {
                    this.hideLoader();
                  });
              });
            }
          }
        ]
      }).then(a => {
        a.present();
      });
    }
  }

  onitemEdit(invId, itemId){
    var object = {
      invId: invId,
      itemId: itemId
    }
    this.navCtrl.navigateForward(["/additem"], { queryParams: object });
  }

  onitemDel(invId, itemId){
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, want you Delete this Item?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Please wait...');
            this.storage.get('userDetails').then((val) => {
              this.data = val;
              var body = {
                source: "mobile",
                user_id: this.data.user_id,
                item_id: itemId,
                invoice_id: invId
              };
              this.authService.postData("invoice/delete/item", body).then(result => {
                this.data = result;
                console.log("Delete: ", this.data);
                this.hideLoader();
                if (this.data.success == true) {
                  this.presentToast("Item has been Deleted Successfully.");
                  this.getDetails();
                } else {
                  this.presentToast("Sorry! " + this.data.message);
                }
              },
                error => {
                  this.hideLoader();
                });
            });
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }

  onDraft() {
    this.presentToast("Invoice Saved as Draft.");
    this.navCtrl.navigateRoot("/invoices");
  }

  onSend() {
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, you want to Send by E-Mail?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Please wait...');
            setTimeout(() => {
              this.storage.get('userDetails').then((val) => {
                this.data = val;
                var body = {
                  source: "mobile",
                  user_id: this.data.user_id,
                  invoice_id: this.invoiceId
                };
                this.authService.postData("invoice/send/mail", body).then(result => {
                  this.data = result;
                  this.hideLoader();
                  if (this.data.success == true) {
                    this.presentToast("Mail has been sent successfully.");
                    this.navCtrl.back();
                  } else {
                    this.presentToast(this.data.message);
                  }
                },
                  error => {
                    this.hideLoader();
                  });
              });
            }, 1000);
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
