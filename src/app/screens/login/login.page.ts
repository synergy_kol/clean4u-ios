import { Component, OnInit } from '@angular/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: any = '';
  password: any = '';

  data: any = [];
  userData: any;

  isDisabled: boolean = false;
  isLoading = false;

  constructor(
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private facebook: Facebook
  ) { }

  onKeyboard(event) {
    if (event != null) {
      event.setFocus();
    } else {
      this.onSignin();
    }
  }

  ngOnInit() {
  }

  onSignin() {
    const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.email.trim() == '') {
      this.presentToast('Please enter your registered Email ID');
    } else if (!emailPattern.test(this.email)) {
      this.presentToast('Wrong Email Format...');
    } else if (this.password.trim() == '') {
      this.presentToast('Please enter Password');
    } else {
      this.showLoader('Please wait...');
      var body = {
        source: "mobile",
        email: this.email,
        password: this.password,
        device_id: ""
      };
      this.authService.postData("Services/login", body).then(result => {
        this.data = result;
        this.userData = this.data.data;
        console.log("Login: ", this.data);
        this.hideLoader();
        if (this.data.success == true) {
          this.storage.set("userDetails", this.userData);
          this.navCtrl.navigateRoot('/welcome');
        } else {
          this.presentToast(this.data.message);
        }
      },
        error => {
          this.hideLoader();
        });
    }
  }

  onFBLogin() {
    this.showLoader('Please wait...');
    this.facebook.login(["public_profile", "email"])
      .then((response: FacebookLoginResponse) => {
        let userId = response.authResponse.userID;
        this.facebook.api("/me?fields=name,email", ["public_profile", "email"])
          .then(user => {
            user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
            console.log("user: ", JSON.stringify(user));
            var body = {
              source: "mobile",
              email: user.email,
              fullname: user.name,
              id: user.id,
              picture: "", //user.picture
            };
            this.authService.postData("api/facebook/signup", body).then(result => {
              this.data = result;
              this.userData = this.data.data;
              console.log("Facebook: ", this.data);
              this.hideLoader();
              if (this.data.success == true) {                
                this.storage.set("userDetails", this.userData);
                this.navCtrl.navigateRoot('/welcome');
              }
            },
              error => {
                this.hideLoader();
              });            
          })
      }, error => {
        this.hideLoader();
      });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
