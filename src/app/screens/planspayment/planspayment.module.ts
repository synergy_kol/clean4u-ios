import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlanspaymentPageRoutingModule } from './planspayment-routing.module';

import { PlanspaymentPage } from './planspayment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlanspaymentPageRoutingModule
  ],
  declarations: [PlanspaymentPage]
})
export class PlanspaymentPageModule {}
