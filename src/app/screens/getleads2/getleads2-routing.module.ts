import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Getleads2Page } from './getleads2.page';

const routes: Routes = [
  {
    path: '',
    component: Getleads2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Getleads2PageRoutingModule {}
