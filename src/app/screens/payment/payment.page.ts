import { Component, OnInit } from '@angular/core';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { PayPal, PayPalConfiguration, PayPalPayment } from '@ionic-native/paypal/ngx';
import { Stripe } from '@ionic-native/stripe/ngx';
import { LoadingController, NavController, Platform, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';
import { ApplePay } from '@ionic-native/apple-pay/ngx';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  data: any = [];
  isLoading = false;
  isDisabled: boolean = false;
  cartsummary: any = "";
  totalpay: any = "";

  payType: any = "stripe";

  cardholdername: any = '';
  cardnumber: any = '';
  expirydate: any = '';
  cvv: any = '';

  userID: any;
  paypalData: any;
  month: any = '';
  year: any = '';
  addId: any = '';

  isShow: boolean = false;

  items: any = [
    {
      label: '3 x Basket Items',
      amount: 49.99
    },
    {
      label: 'Next Day Delivery',
      amount: 3.99
    },
    {
      label: 'My Fashion Company',
      amount: 53.98
    }
  ];
  shippingMethods: any = [
    {
      identifier: 'NextDay',
      label: 'NextDay',
      detail: 'Arrives tomorrow by 5pm.',
      amount: 3.99
    },
    {
      identifier: 'Standard',
      label: 'Standard',
      detail: 'Arrive by Friday.',
      amount: 4.99
    },
    {
      identifier: 'SaturdayDelivery',
      label: 'Saturday',
      detail: 'Arrive by 5pm this Saturday.',
      amount: 6.99
    }
  ];
  supportedNetworks: any = ['visa', 'amex'];
  merchantCapabilities: any = ['3ds', 'debit', 'credit'];
  merchantIdentifier: string = 'merchant.com.Clean4U.metTech';
  currencyCode: string = 'GBP';
  countryCode: string = 'GB';
  billingAddressRequirement: any = ['name', 'email', 'phone'];
  shippingAddressRequirement: any = 'none';
  shippingType: string = "shipping"

  constructor(
    private loadingController: LoadingController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private navCtrl: NavController,
    private toastController: ToastController,
    private payPal: PayPal,
    private stripe: Stripe,
    private iab: InAppBrowser,
    private pltfrm: Platform,
    private applePay: ApplePay,
    private activatedRoute: ActivatedRoute
  ) { 
    this.activatedRoute.queryParams.subscribe((res) => {
      this.addId = res.addressId;
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id
      };
      this.authService.postData("api/product/getcart", body).then(result => {
        this.data = result;
        this.hideLoader();
        if (this.data.data != null) {
          this.cartsummary = this.data.data.summary;
          console.log("Cart Summary: ", this.cartsummary);
          this.totalpay = this.cartsummary.total;
        }
      },
        error => {
          this.hideLoader();
        });
    });

    if(this.pltfrm.is('ios')){
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }

  onPay() {
    if (this.payType == "paypal") {
      this.storage.get('userDetails').then((result) => {
        this.data = result;
        this.userID = this.data.user_id;
        this.payPal.init({
          PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
          PayPalEnvironmentSandbox: 'AbzaJjjdSZ7ub9OZwF_8Ns40vJBy0WCzHeiYzTxA6BmmugGLzgNL_NbBvmaixcXQGc7Bc_O-7ONLMHoJ'
        }).then(() => {
          this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
          })).then(() => {
            let payment = new PayPalPayment((this.totalpay).toString(), 'USD', 'Description', 'sale');
            this.payPal.renderSinglePaymentUI(payment).then((response) => {
              console.log(response);
              this.paypalData = response;
              var body = {
                source: "mobile",
                user_id: this.userID,
                customer_id: "",
                payment_id: this.paypalData.response.id,
                type: 'paypal',
                address_id: this.addId
              };
              this.authService.postData("api/product/payment", body).then(result => {
                this.data = result;
                alert(this.data.success);
                if (this.data.success == true) {
                  this.hideLoader();
                  this.presentToast("Thanks! Payment has been done Successfully.");
                  this.navCtrl.navigateRoot('/success');
                } else {
                  this.presentToast(this.data.message);
                }
              },
                error => {
                  this.hideLoader();
                  this.presentToast(error);
                });
            }, () => {
              this.hideLoader();
            });
          }, () => {
            this.hideLoader();
          });
        }, () => {
          this.hideLoader();
        });
      });
    } else if (this.payType == "stripe") {
      var dateTimeArr = this.expirydate.split('T');
      var dateArr = dateTimeArr[0];
      var newDateArr = String(dateArr).split('-');
      this.month = newDateArr[1];
      this.year = newDateArr[0];
      if (this.cardholdername.trim() == '') {
        this.presentToast('Please enter Cardholder Name');
      } else if (this.cardnumber.trim() == '') {
        this.presentToast('Please enter Card Number');
      } else if (this.expirydate.trim() == '') {
        this.presentToast('Please enter Expiry Date');
      } else if (this.cvv.trim() == '') {
        this.presentToast('Please enter CVV number');
      } else {
        console.log(this.expirydate);
        this.showLoader('Please wait...');
        this.storage.get('userDetails').then((result) => {
          this.data = result;
          this.userID = this.data.user_id;
          this.stripe.setPublishableKey('pk_test_3HWne9ro2LSEHzgJQoTplTJx00d6VVZIv8');
          let card = {
            number: this.cardnumber,
            expMonth: this.month,
            expYear: this.year,
            cvc: this.cvv
          }
          this.stripe.createCardToken(card)
            .then(token => {
              console.log("token: ", token.id);
              var body = {
                source: "mobile",
                user_id: this.userID,
                customer_id: "",
                payment_id: token.id,
                type: 'strip',
                address_id: this.addId
              };
              this.authService.postData("api/product/payment", body).then(result => {
                this.data = result;
                if (this.data.success == true) {
                  this.hideLoader();
                  this.presentToast("Thanks! Payment has been done Successfully.");
                  this.navCtrl.navigateRoot('/success');
                } else {
                  this.presentToast(this.data.message);
                }
              },
                error => {
                  this.hideLoader();
                  this.presentToast(error);
                });
            })
            .catch(error => {
              this.presentToast(error);
              this.hideLoader();
            });
        });
      }
    } else if (this.payType == "gpay") {
      const options: InAppBrowserOptions = {
        zoom: 'no',
        location: 'no',
        /* toolbar: 'no', */
        closebuttoncaption: 'Close',
      };
      const browser = this.iab.create("https://pay.google.com/intl/en_in/about/?gclid=EAIaIQobChMIh86EssnG7wIVBCUrCh3A5wW-EAAYASAAEgJc7fD_BwE&gclsrc=aw.ds", 'system', options);
      browser.on('loadstart').subscribe(event => {
        console.log("url: ", event.url);
      });
    } else {
      var order: any = {
        items: this.items,
        shippingMethods: this.shippingMethods,
        merchantIdentifier: this.merchantIdentifier,
        currencyCode: this.currencyCode,
        countryCode: this.countryCode,
        billingAddressRequirement: this.billingAddressRequirement,
        shippingAddressRequirement: this.shippingAddressRequirement,
        shippingType: this.shippingType,
        merchantCapabilities: this.merchantCapabilities,
        supportedNetworks: this.supportedNetworks
      }
      this.applePay.makePaymentRequest(order).then(message => {
        console.log(message);
        this.applePay.completeLastTransaction('success');
        this.navCtrl.navigateRoot('/success');
      }).catch((error) => {
        this.presentToast(error);
        this.applePay.completeLastTransaction('failure');
      });
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
