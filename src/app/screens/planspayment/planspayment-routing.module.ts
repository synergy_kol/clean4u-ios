import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlanspaymentPage } from './planspayment.page';

const routes: Routes = [
  {
    path: '',
    component: PlanspaymentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlanspaymentPageRoutingModule {}
