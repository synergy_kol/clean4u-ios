import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-supplies',
  templateUrl: './supplies.page.html',
  styleUrls: ['./supplies.page.scss'],
})
export class SuppliesPage implements OnInit {

  data: any = [];
  allproductList: any = [];
  isLoading = false;
  pageNum: any = 10;
  incart: any = "";
  paginatorNo: any = "";

  constructor(
    private loadingController: LoadingController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.data = val;
      var body = {
        source: "mobile",
        user_id: this.data.user_id,
        limit: this.pageNum
      };
      this.authService.postData("api/product/list", body).then(result => {
        this.data = result;
        console.log("Products: ", this.data);
        this.allproductList = this.data.data.data;
        this.paginatorNo = this.data.data.paginator.total_rows;
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });

      var cartqnt = {
        source: "mobile",
        user_id: this.data.user_id
      };
      this.authService.postData("api/product/cartcount", cartqnt).then(result => {
        this.data = result;
        this.incart = this.data.data;
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  doInfinite(event) {
    this.showLoader('Loading...');
    setTimeout(() => {
      this.pageNum += 10;
      this.getDetails();
      event.target.complete();
      console.log(this.allproductList.length, this.paginatorNo);
    }, 500);
  }

  goDetails(Id) {
    var object = {
      itemId: Id
    }
    this.navCtrl.navigateForward(["/supplydetails"], { queryParams: object });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
